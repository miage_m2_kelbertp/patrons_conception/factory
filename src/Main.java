public class Main {

    public static void main(String[] args) {
        PersistKit kit;
        HighScore hs;

        for (int i = 0; i < 4; i++) {
            int random = (int) (Math.random() * 20 + 1);
            if (random < 10) {
                kit = new SRKit();
            } else {
                kit = new JDBCKit();
            }

            hs = kit.makeKit();

            System.out.println("Random number: " + i);
            hs.WhoTheHellAreYou();
        }
    }
}
